#!/bin/bash
# Name: bashconf.sh
# Short-Description: Bash Configurator
# Version: 0.1.9

# Provides: bashconf.sh
# Requires: bash, whiptail, cat, grep, sed, cut

# Devs: Pavel Sibal <entexsoft@gmail.com>

#
# Bash Configurator (bashconf) is script for config functionality and design of
# the Bash prompt.
# Allow to you by the very easy way change your bash prompt, backup and restore
# files of your bash prompt, etc..
#
# Bash Configurator (bashconf) is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# You can get a copy of the license at www.gnu.org/licenses
#
# Bash Configurator (bashconf) is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# <http:/www.gnu.org/licenses/>.

# Run:
# bashconf.sh && source ~/.bashrc

apptitle="Bash Configurator (bashconf) - Version: 0.1.7 (GPLv3)"
WORKING_DIR=$(dirname "$(realpath $0)")
THEMES_PATH=$WORKING_DIR/themes
ALIASES_PATH=$WORKING_DIR/aliases
BACKUP_DIR=~/.backup-bash

selectL18n()
{
  l18n_path="$WORKING_DIR/l18n"
  l18n=${LANG:0:5}
  if [ -e $l18n_path/$l18n ]; then
    source $l18n_path/$l18n
  else
    source $l18n_path/en_US
  fi
  unset l18n_path l18n
}

mainmenu()
{
  if [ "$1" = "" ]; then
      nextitem="."
    else
      nextitem=$1
  fi
  options=()
  options+=("Setup Wizard" "$txtSetupWizardDesc")
  options+=(" " "")
  options+=("Manage" "$txtManageDesc")
  options+=("Xterm" "$txtXtermDesc")
  options+=(" " "")
  options+=("Backup" "$txtBackupFilesDesc")
  options+=("Restore" "$txtRestoreFilesDesc")
  options+=(" " "")
  options+=("Exit" "$txtExitDesc")

  sel=$(whiptail --backtitle "$apptitle" --title "$txtMainMenuHead" --menu "" \
    --cancel-button "$butExit" --default-item "$nextitem" 16 60 9 "${options[@]}" \
    3>&1 1>&2 2>&3)
    
    case $sel in
      ' ')
        mainmenu
      ;;
      'Setup Wizard')
        setupwizard
        managemenu 'Styles'
      ;;
      Manage)
        managemenu
        nextitem='Manage'
      ;;
      Xterm)
		setxterm
		mainmenu
      ;;
      Backup)
        backupfiles
        mainmenu 'Manage'
      ;;
      Restore)
        restorefiles
        mainmenu 'Manage'
      ;;
      *)
        clear
        exit 0
      ;;
    esac
      mainmenu "$nextitem"
}

managemenu()
{
  if [ "$1" = "" ]; then
      nextitem="."
    else
      nextitem=$1
  fi
  options=()
  options+=("Themes" "$txtThemesDesc")
  options+=("Color" "$txtColorDesc")
  options+=("Art" "$txtArdDesc")
  options+=("Aliases" "$txtManageAliasesDesc")
  options+=(".bashrc" "$txtEditBashrcDesc")
  options+=(".bash_aliases" "$txtEditBashAliasesDesc")
  options+=(".bashrc-personal" "$txtEditBashPersonalDesc")
  options+=(" " "")
  options+=("Back" "$txtBackDesc")
  sel=$(whiptail --backtitle "$apptitle" --title "$txtManageMenuHead" --menu "" \
    --cancel-button "$butBack" --default-item "$nextitem" 16 60 9 "${options[@]}" \
    3>&1 1>&2 2>&3)

    case $sel in
      ' ')
        managemenu
      ;;
      Themes)
        changethemes
        managemenu 'Color'
      ;;
      Color)
        changecolor
        managemenu 'Art'
      ;;
      Art)
        artmenu
        managemenu 'Aliases'
      ;;
      Aliases)
        managealiases
        managemenu '.bashrc'
      ;;
      '.bashrc')
        editfile '~/.bashrc'
        managemenu '.bash_aliases'
      ;;
      '.bash_aliases')
        editfile '~/.bash_aliases'
        managemenu '.bashrc-personal'
      ;;
      '.bashrc-personal')
        editfile '~/.bashrc-personal'
        managemenu 'Back'
      ;;
      *)
        return 0
      ;;
    esac
}

managealiases()
{
  options=()
  if [[ -n $(cat ~/.bash_aliases | grep "# Name: standard") ]]; then
    options+=("standard" "$(sed -n 's/\# Description *: //p' $ALIASES_PATH/standard) " ON)
  else
    options+=("standard" "$(sed -n 's/\# Description *: //p' $ALIASES_PATH/standard) " OFF)
  fi

  for x in $(ls -1 $ALIASES_PATH/)
  do
    if [[ $x != "standard" ]]; then
      if [[ -n $(cat ~/.bash_aliases | grep "# Name: $x") ]]; then
        options+=($x "$(sed -n 's/\# Description *: //p' $ALIASES_PATH/$x) " ON)
      else
        options+=($x "$(sed -n 's/\# Description *: //p' $ALIASES_PATH/$x) " OFF)
      fi
    fi
  done

  sel=$(whiptail --title "$txtManageMenuAliasesHead" --checklist "$txtManageMenuAliasesDesc" \
      --cancel-button "Back" 18 75 12 "${options[@]}" 3>&1 1>&2 2>&3)

  cat /dev/null > ~/.bash_aliases
  for x in $sel
  do
    cat $ALIASES_PATH/$(echo $x | sed 's/"//g') >> ~/.bash_aliases
  done
}


changethemes()
{
  if [[ ! $(grep "^color_prompt=" ~/.bashrc) ]]; then
    echo -e "\ncolor_prompt=yes" >> ~/.bashrc
  fi
  if [[ ! $(grep '\..*bash-conf/themes' ~/.bashrc) ]]; then
    echo -e ". $WORKING_DIR/themes/standard" >> ~/.bashrc
  fi
  options=()
  for x in $(ls -1 $THEMES_PATH/)
  do
    options+=($x "$(sed -n 's/\# Description *: //p' $THEMES_PATH/$x) ")
  done
  nextitem="$(cat ~/.bashrc | grep '\..*bash-conf/themes' | rev | cut -d"/" -f1 | rev)"
  if [ "$nextitem" = "" ]; then
      nextitem="."
  fi
  sel=$(whiptail --title "$txtPromptThemeMenuHead" --menu "\n$txtPromptThemeMenuDesc" \
    --cancel-button "$butBack" --default-item "$nextitem" 15 60 8 "${options[@]}" \
    3>&1 1>&2 2>&3)
  if [ "$?" = "0" ]; then
    sed -i "s/^\..*$(echo $THEMES_PATH | sed 's_/_\\/_g')\/.*/\. $(echo $THEMES_PATH | sed 's_/_\\/_g')\/$sel/g" ~/.bashrc
  fi

}

changecolor()
{
  if (whiptail --backtitle "$apptitle" --title "$txtColorMenuHead" --yesno \
    "$txtColorMenuDesc" 8 60); then
    sed -i "s/^color_prompt=.*/color_prompt=yes/g" ~/.bashrc
  else
    sed -i "s/^color_prompt=.*/color_prompt=no/g" ~/.bashrc
  fi
}

artmenu()
{
  options=()
  none_val=ON
  if command -v screenfetch >/dev/null 2>&1; then
    if grep -q "^screenfetch.*" ~/.bashrc; then
      options+=("Screenfetch" "enable/disable screenfetch" ON)
      none_val=OFF
    else
      options+=("Screenfetch" "enable/disable screenfetch" OFF)
    fi
  fi
  if command -v neofetch >/dev/null 2>&1; then
    if grep -q "^neofetch.*" ~/.bashrc; then
      options+=("Neofetch" "enable/disable neofetch" ON)
      none_val=OFF
    else
      options+=("Neofetch" "enable/disable neofetch" OFF)
    fi
  fi
  if command -v linuxlogo >/dev/null 2>&1; then
    if grep -q "^linuxlogo.*" ~/.bashrc; then
      options+=("Linux_logo" "enable/disable linux_logo" ON)
      none_val=OFF
    else
      options+=("Linux_logo" "enable/disable linux_logo" OFF)
    fi
  fi
  if command -v archey >/dev/null 2>&1; then
    if grep -q "^archey.*" ~/.bashrc; then
      options+=("Archey" "enable/disable archey" ON)
      none_val=OFF
    else
      options+=("Archey" "enable/disable archey" OFF)
    fi
  fi
  if command -v alsi >/dev/null 2>&1; then
    if grep -q "^alsi.*" ~/.bashrc; then
      options+=("Alsi" "enable/disable alsi" ON)
      none_val=OFF
    else
      options+=("Alsi" "enable/disable alsi" OFF)
    fi
  fi
  options=("None" "$txtArtNone" $none_val "${options[@]}")
  sel=$(whiptail --backtitle "$apptitle" --title "$txtArtMenuHead" --radiolist \
    "$txtArtMenuDesc" --cancel-button "$butBack" 15 60 8 "${options[@]}" \
    3>&1 1>&2 2>&3)
  if [ "$?" = "0" ]; then
    sed -i "/^screenfetch.*/d" ~/.bashrc
    sed -i "/^neofetch.*/d" ~/.bashrc
    sed -i "/^linuxlogo.*/d" ~/.bashrc
    sed -i "/^archey.*/d" ~/.bashrc
    sed -i "/^alsi.*/d" ~/.bashrc
    case $sel in
      Screenfetch)
        echo -e "\nscreenfetch" >> ~/.bashrc
      ;;
      Neofetch)
        echo -e "\nneofetch" >> ~/.bashrc
      ;;
      Linux_logo)
        echo -e "\nlinuxlogo" >> ~/.bashrc
      ;;
      Archey)
        echo -e "\narchey" >> ~/.bashrc
      ;;
      Alsi)
        echo -e "\nalsi" >> ~/.bashrc
    esac
  fi
}

backupfiles()
{
  actdate=$(date +%Y%m%d_%H-%M-%S)
  [ -d $BACKUP_DIR ] || mkdir -p $BACKUP_DIR
  mkdir $BACKUP_DIR/backup-bash.$actdate
  [[ -f ~/.bashrc ]] && cat ~/.bashrc > $BACKUP_DIR/backup-bash.$actdate/.bashrc
  [[ -f ~/.bash_aliases ]] && cat ~/.bash_aliases > $BACKUP_DIR/backup-bash.$actdate/.bash_aliases
  [[ -f ~/.bashrc-personal ]] && cat ~/.bashrc-personal > $BACKUP_DIR/backup-bash.$actdate/.bashrc-personal
  [[ -f ~/.Xresources ]] && cat ~/.Xresources > $BACKUP_DIR/backup-bash.$actdate/.Xresources
  cd $BACKUP_DIR
  tar czf $BACKUP_DIR/backup-bash.$actdate.tar.gz backup-bash.$actdate/
  rm -rf $BACKUP_DIR/backup-bash.$actdate
  cd $WORKING_DIR
}

restorefiles()
{
  if [ "$1" = "" ]; then
      nextitem="."
    else
      nextitem=$1
  fi
  [ -d $BACKUP_DIR ] || mkdir -p $BACKUP_DIR
  options=("None" "$txtRestoreNone" ON)
  for x in $(ls -1r $BACKUP_DIR/backup-bash.* | cut -d"." -f3)
  do
    options+=($x "$txtBackupFrom $(date -d "$(echo $x | sed 's/_/ /g' | sed 's/-/:/g')" +'%d.%m.%Y %H:%M:%S')" OFF)
  done
  sel=$(whiptail --backtitle "$apptitle" --title "$txtRestoreMenuHead" --radiolist \
    "$txtRestoreMenuDesc" --cancel-button "$butBack" 15 60 8 "${options[@]}" \
    3>&1 1>&2 2>&3)
    if [[ $sel != "None" ]] && [[ $sel != '' ]]; then
        if (whiptail --backtitle "$apptitle" --title "$txtRestoreFiles" --yesno \
            "$txtRestoreFilesQuestion1 $(date -d "$(echo $x | sed 's/_/ /g' | \
            sed 's/-/:/g')" +'%d.%m.%Y %H:%M:%S')?\n$txtRestoreFilesQuestion2" \
          8 78); then
          tar -xf $BACKUP_DIR/backup-bash.$sel.tar.gz -C $BACKUP_DIR/
          [[ -f $BACKUP_DIR/backup-bash.$sel/.bashrc ]] && cat $BACKUP_DIR/backup-bash.$sel/.bashrc > ~/.bashrc
          [[ -f $BACKUP_DIR/backup-bash.$sel/.bash_aliases ]] && cat $BACKUP_DIR/backup-bash.$sel/.bash_aliases > ~/.bash_aliases
          [[ -f $BACKUP_DIR/backup-bash.$sel/.bashrc-personal ]] && cat $BACKUP_DIR/backup-bash.$sel/.bashrc-personal > ~/.bashrc-personal
          [[ -f $BACKUP_DIR/backup-bash.$sel/.Xresources ]] && cat $BACKUP_DIR/backup-bash.$sel/.Xresources > ~/.Xresources
          rm -rf $BACKUP_DIR/backup-bash.$sel
        fi
    fi
}

setupwizard()
{
  if (whiptail --backtitle "$apptitle" --title "$txtSetupWizardMenuHead" --yesno \
    "$txtSetupWizardMenuDesc" 15 60); then
    backupfiles
    cat $WORKING_DIR/resources/bashrc > ~/.bashrc
    sed -i "s|/usr/share/bash-conf/themes/standard|$WORKING_DIR/themes/standard|" ~/.bashrc
    cat $ALIASES_PATH/standard > ~/.bash_aliases
    cat $WORKING_DIR/resources/bashrc-personal > ~/.bashrc-personal
    whiptail --title "$txtSetupWizardMenuHead" --msgbox "$txtSetupWizardFinishDesc" 8 78
  fi
  return 0
}

editfile()
{
  if [[ -z $EDITOR ]]; then
    nano $1
  else
    $EDITOR $1
  fi
}

setxterm()
{
  if (whiptail --backtitle "$apptitle" --title "$txtSetXtermHead" --yesno \
    "$txtSetXtermDesc" 8 50); then
	cat $WORKING_DIR/resources/Xresources > ~/.Xresources
	mkdir -p ~/.local/share/applications/
	cat $WORKING_DIR/resources/xterm-grey.desktop > ~/.local/share/applications/xterm-grey.desktop
	cat $WORKING_DIR/resources/xterm-black.desktop > ~/.local/share/applications/xterm-black.desktop
	cat $WORKING_DIR/resources/xterm-navyblue.desktop > ~/.local/share/applications/xterm-navyblue.desktop
  fi
}

selectL18n
mainmenu

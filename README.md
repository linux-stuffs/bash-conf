# Bash Configurator (bashconf)

Bash Configurator (bashconf) is script for config functionality and design of
the Bash prompt.

Allow to you by the very easy way change your bash prompt, backup and restore
files of your bash prompt, etc..

![wp-01-simple](img-git/bashconf01.png)

![wp-01-simple](img-git/bashconf02.png)

![wp-01-simple](img-git/bashconf03.png)

![wp-01-simple](img-git/bashconf04.png)

![wp-01-simple](img-git/bashconf05.png)

![wp-01-simple](img-git/bashconf06.png)

![wp-01-simple](img-git/bashconf07.png)

## BASH PROMPT THEMES

Atomic prompt theme
![Atomic prompt theme](img-git/theme-atomic.png)

Axin prompt theme
![Axin prompt theme](img-git/theme-axin.png)

Bakke prompt theme
![Bakke prompt style](img-git/theme-bakke.png)

Bobby prompt theme
![Bobby prompt theme](img-git/theme-bobby.png)

Blue colored box line prompt
![Blue colored box line prompt](img-git/theme-box_blue.png)

Red colored box line prompt
![Red colored box line prompt](img-git/theme-box_red.png)

Debian-like prompt theme
![Debian-like prompt theme](img-git/theme-debian.png)

DOS-like prompt theme
![DOS-like prompt theme](img-git/theme-dos.png)

Doubletime prompt theme
![Doubletime prompt theme](img-git/theme-doubletime.png)

Elive OS prompt theme
![Elive OS prompt theme](img-git/theme-elive.png)

Kitsune prompt theme
![Kitsune prompt theme](img-git/theme-kitsune.png)

Mairan prompt theme
![Mairan prompt theme](img-git/theme-mairan.png)

Mr.Briggs prompt theme
![Mr.Briggs prompt theme](img-git/theme-mbriggs.png)

Minimal prompt theme
![Minimal prompt theme](img-git/theme-minimal.png)

Norbu prompt theme
![Norbu prompt theme](img-git/theme-norbu.png)

Nwinkler prompt theme
![Nwinkler prompt theme](img-git/theme-nwinkler.png)

Parrot OS prompt theme
![Parrot OS prompt theme](img-git/theme-parrot.png)

Simple green/blue prompt theme
![Simple green/blue prompt theme](img-git/theme-simple_gb.png)

Simple prompt theme colored like Parrot OS
![](img-git/theme-simple_par.png)

Standard green/white/green prompt
![Standard green/white/green prompt](img-git/theme-standard_gwg.png)

Standard simple prompt style
![Standard simple prompt style](img-git/theme-standard.png)

Zork prompt theme
![Zork prompt style](img-git/theme-zork.png)

90210 prompt theme
![90210 prompt style](img-git/theme-90210.png)

## INSTALLATION

### From the [AUR](https://aur.archlinux.org/packages/bash-conf/):
```
git clone https://aur.archlinux.org/bash-conf.git
cd bash-conf/
makepkg -sci
```

### Install from source:
Unpack the source package and run command (like root):
```
make install
```

### Uninstall from source:
Run command (like root):
```
make uninstall
```

### Build own ArchLinux package
You need these packages for building the ArchLinux package:
```
base-devel git wget yajl
```

Run command:
```
make build-arch
```

### Install from *.pkg.tar.xz package:
Run command (like root):
```
pacman -U distrib/bash-conf*.pkg.tar.xz
```


### Build own Debian package
You need these packages for building the Debian package:
```
build-essential make fakeroot util-linux debianutils lintian dpkg coreutils
```

Run command (like root):
```
make build-deb
```

### Install from *.deb package:
```
apt install ./distrib/bash-conf*.deb
```
